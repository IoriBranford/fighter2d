#ifndef __FINT_H
#define __FINT_H

#include <stdint.h>

const int FINT_FRAC_SIZE = 8;

typedef int fint;
typedef int16_t fint16;
typedef int32_t fint32;
typedef int64_t fint64;

#define fint2int(x) ((x) >> FINT_FRAC_SIZE)
#define int2fint(x) ((x) << FINT_FRAC_SIZE)

#endif