#include "f2d.h"
#include "f2d_Fighter.hpp"

#include "mugenfile.h"
#include <sstream>
#include <iostream>
#include <stdint.h>
#include <fint.h>

using namespace std;

struct f2d_FightState {
	unsigned int timepassed;
	fint leftwall, rightwall;
	f2d_FighterState fighters[NUM_FIGHTERS];
	f2d_FighterState projectiles[2];

	f2d_FightState(fint Leftwall = -int2fint(256), fint Rightwall = int2fint(256)) :
		timepassed(0), leftwall(Leftwall), rightwall(Rightwall) {}
};

struct f2d_Fight {
	unsigned int timelimit;
	f2d_FightState fightstate;
	f2d_FighterData fighterdata[NUM_FIGHTERS];

	f2d_Fight(unsigned int Timelimit = 6000) : timelimit(Timelimit) {}
};

f2d_Fight* f2d_CreateFight() {
	cout << "sizeof(f2d_FightState) " << sizeof(f2d_FightState) << endl;
	f2d_Fight *fight = new f2d_Fight();

	f2d_FightState &fs = fight->fightstate;
	fs.fighters[0].x = -int2fint(128);
	fs.fighters[1].x = int2fint(128);
	fs.fighters[0].y = int2fint(256);
	fs.fighters[1].y = int2fint(256);
	fs.fighters[1].face = -1;

	return fight;
}

void f2d_LoadFighterData(f2d_Fight *fight, int i, const char *fighterpath) {
	stringstream ss;
	ss << fighterpath << ".air";
	fight->fighterdata[i].actions = MUGEN_LoadActions(ss.str().c_str());
	ss.clear();
}

void f2d_FreeFight(f2d_Fight* fight) {
	for (int i : {0, 1}) {
		const MUGEN_ActionDict* ad = fight->fighterdata[i].actions;
		if (ad)
			MUGEN_FreeActions(ad);
	}
	delete fight;
}

static void ControlFighter(f2d_FighterState& fers, int joystick, int buttons) {
	int face = fers.face;
	joystick *= face;

	if (abs(joystick) == f2d_JOY_D || joystick == f2d_JOY_DB) {
		++fers.downcharge;
	}
	else if (fers.downcharge > 0) {
		--fers.downcharge;
	}

	if (f2d_JOY_DB >= joystick && joystick >= f2d_JOY_UB) {
		++fers.backcharge;
	}
	else if (fers.backcharge > 0) {
		--fers.backcharge;
	}
}

static void UpdateFighterAction(f2d_FighterState& fers, const MUGEN_Action* action) {
	++fers.frametimer;
	int frameidx = MUGEN_CalcNextActionFrame(action, fers.frameidx, fers.frametimer);
	if (frameidx != fers.frameidx) {
		fers.frametimer = 0;
	}
}

void f2d_UpdateFight(f2d_Fight* fight, int joy0, int buttons0, int joy1, int buttons1) {
	f2d_FightState& fs = fight->fightstate;
	f2d_FighterState& fers0 = fs.fighters[0];
	f2d_FighterState& fers1 = fs.fighters[1];

	const MUGEN_ActionDict* ad0 = fight->fighterdata[0].actions;
	const MUGEN_ActionDict* ad1 = fight->fighterdata[1].actions;

	bool timeover = fs.timepassed >= fight->timelimit;

	int inputi = fs.timepassed % INPUT_BUFFER_LENGTH;
	fers0.joybuffer[inputi] = joy0;
	fers0.buttonsbuffer[inputi] = buttons0;
	fers1.joybuffer[inputi] = joy1;
	fers1.buttonsbuffer[inputi] = buttons1;

	ControlFighter(fers0, joy0, buttons0);
	ControlFighter(fers1, joy1, buttons1);

	const MUGEN_Action* a0 = MUGEN_GetAction(ad0, fers0.actionid);
	const MUGEN_Action* a1 = MUGEN_GetAction(ad1, fers1.actionid);

	if (a0) {
		UpdateFighterAction(fers0, a0);
	}

	if (a1) {
		UpdateFighterAction(fers1, a1);
	}

	if (a0 && a1) {
		const MUGEN_Frame* f0 = MUGEN_GetActionFrame(a0, fers0.frameidx);
		const MUGEN_Frame* f1 = MUGEN_GetActionFrame(a1, fers1.frameidx);
		int collisionresult = MUGEN_TestCollision(f0, fers0.x, fers0.y, fers0.face, f1, fers1.x, fers1.y, fers1.face);
	}

	++fs.timepassed;
}

f2d_FightState* f2d_CopyFightState(f2d_Fight* fight) {
	return new f2d_FightState(fight->fightstate);
}

void f2d_SetFightState(f2d_Fight* fight, f2d_FightState* fightstate) {
	fight->fightstate = *fightstate;
}

void f2d_FreeFightState(f2d_FightState* fightstate) {
	delete fightstate;
}
