#include "f2d_Fighter.hpp"
#include "mugenfile.h"

void f2d_FighterChangeAnim(f2d_FighterState *fs, int animid, int frameidx) {
    fs->actionid = animid;
    fs->frameidx = frameidx;
    fs->frametimer = 0;
}

void f2d_FighterChangeState(f2d_FighterState *fs, int stateid) {
    fs->prevstateid = fs->stateid;
    fs->stateid = stateid;
    fs->statetime = 0;
}