#ifndef __FIGHTER2D_H
#define __FIGHTER2D_H

#ifdef __cplusplus
extern "C" {
#endif

	const int NUM_FIGHTERS = 2;

	enum f2d_Joystick {
		f2d_JOY_N	= 0,
		f2d_JOY_D	= 1,
		f2d_JOY_DF	= 2,
		f2d_JOY_DB	= -2,
		f2d_JOY_F	= 3,
		f2d_JOY_B	= -3,
		f2d_JOY_UF	= 4,
		f2d_JOY_UB	= -4,
		f2d_JOY_U	= 5
	};

	enum f2d_Stance {
		f2d_STANCE_STAND = 'S',
		f2d_STANCE_CROUCH = 'C',
		f2d_STANCE_AIR = 'A',
		f2d_STANCE_DOWN = 'D'
	};

	typedef struct f2d_Fight f2d_Fight;
	typedef struct f2d_FightState f2d_FightState;
	typedef struct f2d_FighterState f2d_FighterState;
	
	f2d_Fight*	f2d_CreateFight();
	void		f2d_FreeFight(f2d_Fight* fight);
	void		f2d_LoadFighterData(f2d_Fight* fight, int i, const char* fighterpath);

	void		f2d_UpdateFight(f2d_Fight* fight, int joy0, int buttons0, int joy1, int buttons1);

	f2d_FightState*	f2d_CopyFightState(f2d_Fight* fight);
	void			f2d_SetFightState(f2d_Fight* fight, f2d_FightState* fightstate);
	void			f2d_FreeFightState(f2d_FightState* fightstate);

	void	f2d_FighterChangeAnim(f2d_FighterState *fs, int animid, int frameidx);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif