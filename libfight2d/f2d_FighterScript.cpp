#include "f2d.h"
#include "f2d_Fighter.hpp"
#include "mugenfile.h"
#include "lua.hpp"

#define lua_registerfunc(L, f) lua_register(L, #f, f)
#define lua_setglobalboolean(L, name, i) (lua_pushboolean(L, i), lua_setglobal(L, name))
#define lua_setglobalinteger(L, name, i) (lua_pushinteger(L, i), lua_setglobal(L, name))

static f2d_FighterState *currentfighterstate = NULL;
static const f2d_FighterData *currentfighterdata = NULL;

void RunFighterScript(lua_State *L, f2d_FighterState *fs, const f2d_FighterData *fd) {
    currentfighterstate = fs;
    currentfighterdata = fd;
    lua_setglobalinteger(L, "Anim", fs->actionid);
    lua_setglobalinteger(L, "Time", fs->statetime);
    lua_setglobalinteger(L, "Health", fs->health);
    // run main script
}

static int ChangeAnim(lua_State *L) {
    int animid = luaL_optint(L, 1, currentfighterstate->actionid);
    const MUGEN_Action *action = MUGEN_GetAction(currentfighterdata->actions, animid);
    if (action) {
        int frameidx = luaL_optint(L, 2, 0);
        f2d_FighterChangeAnim(currentfighterstate, animid, frameidx);
    }
    return 0;
}

static int VelSet(lua_State *L) {
    fint velx = luaL_optint(L, 1, currentfighterstate->velx);
    fint vely = luaL_optint(L, 2, currentfighterstate->vely);
    currentfighterstate->velx = velx;
    currentfighterstate->vely = vely;
    return 0;
}

lua_State* FighterScriptingInit() {
    currentfighterstate = NULL;
    currentfighterdata = NULL;
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);
    lua_registerfunc(L, ChangeAnim);
    lua_registerfunc(L, VelSet);
    return L;
}

void FighterScriptingFree(lua_State *L) {
    lua_close(L);
}

