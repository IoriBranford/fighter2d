#ifndef __f2d_FighterState_hpp
#define __f2d_FighterState_hpp

#include <cstddef>
#include <fint.h>
#include <stdint.h>

struct MUGEN_ActionDict;

const int INPUT_BUFFER_LENGTH = 32;

struct f2d_FighterState {
	int8_t joybuffer[INPUT_BUFFER_LENGTH];
	int8_t buttonsbuffer[INPUT_BUFFER_LENGTH];

	// 16-byte boundary (default for windows x64)

	fint32 x, y;
	fint16 velx, vely;
	uint16_t health;
	uint16_t stun;

	// 16-byte boundary

	uint16_t actionid;
	uint8_t frameidx;
	uint8_t frametimer;
	int8_t face; // 1 or -1
	int8_t stance;
	uint8_t backcharge, downcharge;

	uint16_t stateid;
	uint16_t prevstateid;
	uint16_t statetime;

	f2d_FighterState(int Health = 1000, int Stun = 1000, fint X = 0, fint Y = 0, int Face = 1, int Actionid = 0) :
		health(Health), stun(Stun), x(X), y(Y), face(Face), actionid(Actionid), frameidx(0), frametimer(0), joybuffer{ 0 }, buttonsbuffer{ 0 },
		downcharge(0), backcharge(0) {}
};

struct f2d_FighterData {
	const MUGEN_ActionDict *actions;
	// later sprites, sounds, commands, properties, etc

	f2d_FighterData(): actions(NULL) {};
};

#endif