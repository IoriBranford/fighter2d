#ifndef __MUGENFILE_H
#define __MUGENFILE_H

#include <fint.h>

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct MUGEN_ActionDict MUGEN_ActionDict;
	typedef struct MUGEN_Action MUGEN_Action;
	typedef struct MUGEN_Frame MUGEN_Frame;
	typedef struct MUGEN_Hitbox {
		fint x0, y0, x1, y1;
	} MUGEN_Hitbox;

	enum MUGEN_FlipFlag {
		FLIP_H,
		FLIP_V
	};

	enum MUGEN_HitboxType {
		HITBOX_NONE,
		HITBOX_ATTACK,
		HITBOX_HURT
	};

	const MUGEN_ActionDict* MUGEN_LoadActions(const char* filename);
	const MUGEN_ActionDict* MUGEN_ReadActions(const char* text);
	void MUGEN_FreeActions(const MUGEN_ActionDict* actions);

	const MUGEN_Action* MUGEN_GetAction(const MUGEN_ActionDict* actiondict, int actionid);
	const MUGEN_Frame* MUGEN_GetActionFrame(const MUGEN_Action* action, int frameidx);
	const MUGEN_Frame* MUGEN_GetFrame(const MUGEN_ActionDict* actiondict, int actionid, int frameidx);
	
	int MUGEN_CalcNextActionFrame(const MUGEN_Action* action, int frameidx, int frametimer);
	int MUGEN_CalcNextFrame(const MUGEN_ActionDict* actiondict, int actionid, int frameidx, int frametimer);

	const MUGEN_Hitbox* MUGEN_GetAttackCollision(const MUGEN_Frame *frame);
	const MUGEN_Hitbox* MUGEN_GetHurtCollision(const MUGEN_Frame* frame);
	int MUGEN_TestCollision(const MUGEN_Frame* f0, fint x0, fint y0, int face0, const MUGEN_Frame* f1, fint x1, fint y1, int face1);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif