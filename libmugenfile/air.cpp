#include "mugenfile.h"

#include <vector>
#include <unordered_map>
#include <regex>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdint.h>

using namespace std;

struct MUGEN_Frame {
	int16_t spritegrp, spriteid;
	int16_t offsetx, offsety;
	int32_t duration;
	uint32_t flags;
	vector<MUGEN_Hitbox>* attackcollision;
	vector<MUGEN_Hitbox>* hurtcollision;

	MUGEN_Frame(int Spritegrp = 0, int Spriteid = 0, int Offsetx = 0, int Offsety = 0, int Duration = 0, int Flags = 0)
		: spritegrp(Spritegrp), spriteid(Spriteid), offsetx(Offsetx), offsety(Offsety), duration(Duration), flags(Flags),
		attackcollision(NULL), hurtcollision(NULL) {}
};

struct MUGEN_Action {
	//int id;
	size_t loopstartframe;
	vector<MUGEN_Frame> frames;

	MUGEN_Action() :
		//id(-1),
		loopstartframe(0),
		frames() {}
};

struct MUGEN_ActionDict {
	unordered_map<int, MUGEN_Action> actions;
	vector<vector<MUGEN_Hitbox>> collisions;

	MUGEN_ActionDict() : actions(), collisions() {}
};

static const regex RX_LINE("^.*$");
static const regex RX_ACTION("\\[Begin Action ([[:d:]]+)\\]");
static const regex RX_CLSN("Clsn([[:d:]])(Default)?:([[:d:]]+)");
static const regex RX_CLSNBOX("[[:s:]]*Clsn[[:d:]]\\[([[:d:]]+)\\] = ([-[:d:]]+),[[:s:]]*([-[:d:]]+),[[:s:]]*([-[:d:]]+),[[:s:]]*([-[:d:]]+)");
static const regex RX_FRAME("([-[:d:]]+),[[:s:]]*([-[:d:]]+),[[:s:]]*([-[:d:]]+),[[:s:]]*([-[:d:]]+),[[:s:]]*([-[:d:]]+),?[[:s:]]*([[:w:]]*)");
static const regex RX_LOOPSTART("loopstart", regex_constants::icase);

static const MUGEN_ActionDict* ReadAIRStream(istream& is) {
	char line[256];

	MUGEN_ActionDict* air = new MUGEN_ActionDict();
	MUGEN_Action* currentaction = NULL;
	vector<MUGEN_Hitbox>* currentcollision = NULL;
	vector<MUGEN_Hitbox>* currentattackcollision = NULL;
	vector<MUGEN_Hitbox>* currenthurtcollision = NULL;
	vector<MUGEN_Hitbox>* defaultattackcollision = NULL;
	vector<MUGEN_Hitbox>* defaulthurtcollision = NULL;

	while (is.good()) {
		is.getline(line, 256);
		cmatch match;

		if (regex_match(line, match, RX_ACTION)) {
			int id = stoi(match[1].str());
			air->actions[id] = MUGEN_Action();
			currentaction = &air->actions[id];
			currentcollision = NULL;
			defaultattackcollision = NULL;
			defaulthurtcollision = NULL;
			//cout << line << " -> " << id << endl;
		}
		else if (regex_match(line, match, RX_CLSN)) {
			if (!currentaction) {
				cerr << "Collision line without an action: " << match[0].str() << endl;
				continue;
			}

			int type = stoi(match[1]);
			bool dflt = match[2].length() > 0;
			int count = stoi(match[3]);
			air->collisions.push_back(vector<MUGEN_Hitbox>());
			currentcollision = &air->collisions.back();
			currentcollision->reserve(count);

			switch (type) {
			case HITBOX_ATTACK:
				currentattackcollision = currentcollision;
				if (dflt) defaultattackcollision = currentcollision;
				break;
			case HITBOX_HURT:
				currenthurtcollision = currentcollision;
				if (dflt) defaulthurtcollision = currentcollision;
				break;
			}

			//cout << line << " -> " << type << "," << dflt << "," << count << endl;
		}
		else if (regex_match(line, match, RX_CLSNBOX)) {
			if (!currentcollision) {
				cerr << "Collision box line without a collision: " << match[0].str() << endl;
				continue;
			}
			currentcollision->push_back(MUGEN_Hitbox());
			MUGEN_Hitbox* hitbox = &currentcollision->back();

			int i = stoi(match[1]);
			int x0 = stoi(match[2]);
			int y0 = stoi(match[3]);
			int x1 = stoi(match[4]);
			int y1 = stoi(match[5]);
			hitbox->x0 = int2fint(x0);
			hitbox->y0 = int2fint(y0);
			hitbox->x1 = int2fint(x1);
			hitbox->y1 = int2fint(y1);
			if (hitbox->y0 > hitbox->y1) {
				swap(hitbox->y0, hitbox->y1);
			}

			//cout << line << " -> " << i << "," << hitbox->x0 << "," << hitbox->y0 << "," << hitbox->x1 << "," << hitbox->y1 << endl;
		}
		else if (regex_match(line, match, RX_FRAME)) {
			if (!currentaction) {
				cerr << "Frame line without an action: " << match[0].str() << endl;
				continue;
			}

			int spritegrp = stoi(match[1]);
			int spriteid = stoi(match[2]);
			int offsetx = stoi(match[3]);
			int offsety = stoi(match[4]);
			int duration = stoi(match[5]);

			string flagstr = match.size() > 6 ? match[6] : string();

			currentaction->frames.push_back(MUGEN_Frame(spritegrp, spriteid, offsetx, offsety, duration, 0));
			MUGEN_Frame* frame = &currentaction->frames.back();
			if (currentattackcollision) {
				frame->attackcollision = currentattackcollision;
				currentattackcollision = NULL;
			}
			else {
				frame->attackcollision = defaultattackcollision;
			}

			if (currenthurtcollision) {
				frame->hurtcollision = currenthurtcollision;
				currenthurtcollision = NULL;
			}
			else {
				frame->hurtcollision = defaulthurtcollision;
			}

			//cout << line << " -> " << frame->spritegrp << "," << frame->spriteid << "," << frame->offsetx << "," << frame->offsety << "," << frame->duration << "," << flagstr << endl;
		}
		else if (regex_match(line, match, RX_LOOPSTART)) {
			if (!currentaction) {
				cerr << "Loopstart without an action: " << match[0].str() << endl;
				continue;
			}
			currentaction->loopstartframe = currentaction->frames.size();
			//cout << line << " -> " << currentaction->loopstartframe << endl;
		}
	}

	return air;
}

const MUGEN_ActionDict* MUGEN_LoadActions(const char *filename) {
	ifstream ifs (filename);
	if (!ifs.good()) {
		cerr << strerror(errno) << endl;
		return NULL;
	}
	const MUGEN_ActionDict* air = ReadAIRStream(ifs);
	ifs.close();
	return air;
}

const MUGEN_ActionDict* MUGEN_ReadActions(const char* text) {
	stringstream is(text);
	return ReadAIRStream(is);
}


void MUGEN_FreeActions(const MUGEN_ActionDict* actions) {
	delete actions;
}

const MUGEN_Action* MUGEN_GetAction(const MUGEN_ActionDict* actiondict, int actionid) {
	auto& actions = actiondict->actions;
	auto it = actions.find(actionid);
	if (it != actions.end()) {
		return &it->second;
	}
	return NULL;
}

const MUGEN_Frame* MUGEN_GetActionFrame(const MUGEN_Action* action, int frameidx) {
	return &action->frames[frameidx];
}

const MUGEN_Frame* MUGEN_GetFrame(const MUGEN_ActionDict* actiondict, int actionid, int frameidx) {
	auto& actions = actiondict->actions;
	auto it = actions.find(actionid);
	if (it != actions.end()) {
		auto& action = it->second;
		return &action.frames[frameidx];
	}
	return NULL;
}

int MUGEN_CalcNextActionFrame(const MUGEN_Action* action, int frameidx, int frametimer) {
	auto& frame = action->frames[frameidx];
	if (frame.duration >= 0 && frametimer >= frame.duration) {
		++frameidx;
		if (frameidx >= action->frames.size()) {
			frameidx = action->loopstartframe;
		}
	}
	return frameidx;
}

int MUGEN_CalcNextFrame(const MUGEN_ActionDict* actiondict, int actionid, int frameidx, int frametimer) {
	auto& actions = actiondict->actions;
	auto it = actions.find(actionid);
	if (it != actions.end()) {
		return MUGEN_CalcNextActionFrame(&it->second, frameidx, frametimer);
	}
	return frameidx;
}

const MUGEN_Hitbox* MUGEN_GetAttackCollision(const MUGEN_Frame* frame) {
	auto attackcollision = frame->attackcollision;
	return attackcollision ? attackcollision->data() : NULL;
}

const MUGEN_Hitbox* MUGEN_GetHurtCollision(const MUGEN_Frame* frame) {
	auto hurtcollision = frame->hurtcollision;
	return hurtcollision ? hurtcollision->data() : NULL;
}

static bool TestHitboxes(const vector<MUGEN_Hitbox>& ac, fint ax, fint ay, int aface, const vector<MUGEN_Hitbox>& hc, fint hx, fint hy, int hface) {
	for (auto & attackbox : ac) {
		for (auto & hurtbox : hc) {
			fint ax0 = ax + attackbox.x0 * aface;
			fint ax1 = ax + attackbox.x1 * aface;
			fint ay0 = ay + attackbox.y0;
			fint ay1 = ay + attackbox.y1;
			fint hx0 = hx + hurtbox.x0 * hface;
			fint hx1 = hx + hurtbox.x1 * hface;
			fint hy0 = hy + hurtbox.y0;
			fint hy1 = hy + hurtbox.y1;
			if (ax0 > ax1) {
				swap(ax0, ax1);
			}
			if (hx0 > hx1) {
				swap(hx0, hx1);
			}
			if (ax1 > hx0 && ay1 > hy0 && hx1 > ax0 && hy1 > ay0)
				return true;
		}
	}
	return false;
}

int MUGEN_TestCollision(const MUGEN_Frame *f0, fint x0, fint y0, int face0, const MUGEN_Frame *f1, fint x1, fint y1, int face1) {
	const vector<MUGEN_Hitbox>* ac0 = f0->attackcollision;
	const vector<MUGEN_Hitbox>* ac1 = f1->attackcollision;

	const vector<MUGEN_Hitbox>* hc0 = f0->hurtcollision;
	const vector<MUGEN_Hitbox>* hc1 = f1->hurtcollision;

	int result = 0;

	if (ac0 && hc1) {
		if (TestHitboxes(*ac0, x0, y0, face0, *hc1, x1, y1, face1)) {
			result &= 1;
		}
	}

	if (ac1 && hc0) {
		if (TestHitboxes(*ac1, x1, y1, face1, *hc0, x0, y0, face0)) {
			result &= 2;
		}
	}

	return result;
}