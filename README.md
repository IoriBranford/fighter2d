# A 2D VS fighting engine

## What I want

- Sprites and hitboxes via MUGEN AIR and SFF files
  - Can make with the free Fighter Factory tool
- Fighter input & state logic via Lua scripts
  - Scripting API providing a subset of MUGEN state commands
- Online play with rollback via GGPO

## Setup for development

### Windows

1. Install vcpkg https://vcpkg.io
  1. Add VCPKG_ROOT environment variable, set it to the vcpkg install location
1. Install SDL2 and LuaJIT with vcpkg `vcpkg.exe install SDL2:x64-windows luajit:x64-windows`
1. Install Visual Studio 2017 or later https://visualstudio.microsoft.com/downloads/
  - Select:
    - "Game Development with C++" workload
    - If you will develop in VS, CMake tools individual component
1. Open project folder in VS or VSCode
1. If using VSCode, install extensions:
  - C/C++
  - CMake
  - CMake Tools

### Mac OSX, Linux and others

To do when GGPO is ported to those platforms.