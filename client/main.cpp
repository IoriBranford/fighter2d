﻿#include <SDL2/SDL.h>
#include "f2d.h"

int main(int nargs, char **args)
{
	SDL_Renderer* renderer = NULL;
	SDL_Window* window = NULL;
	SDL_CreateWindowAndRenderer(720, 480, 0, &window, &renderer);
	
	f2d_Fight* fight = f2d_CreateFight();
	f2d_LoadFighterData(fight, 0, "data/characters/seruzad/seruzad");
	f2d_LoadFighterData(fight, 1, "data/characters/seruzad/seruzad");

	Uint64 counter = SDL_GetPerformanceCounter();
	Uint64 fixeddeltafrequency = SDL_GetPerformanceFrequency() / 60;
	Uint64 fixeddeltacounter = 0;
	bool running = true;
	while (running) {
		SDL_Event e;
		while (SDL_PollEvent(&e)) {
			switch (e.type) {
			case SDL_QUIT:
				running = false;
				break;
			}
		}

		Uint64 nextcounter = SDL_GetPerformanceCounter();
		Uint64 deltacounter = nextcounter - counter;
		counter = nextcounter;
		fixeddeltacounter += deltacounter;
		while (fixeddeltacounter >= fixeddeltafrequency) {
			f2d_UpdateFight(fight, 0, 0, 0, 0);
			fixeddeltacounter -= fixeddeltafrequency;
		}

		Uint64 deltamsec = 1000 * deltacounter / SDL_GetPerformanceFrequency();
		// variable update

		SDL_RenderClear(renderer);
		// render
		SDL_RenderPresent(renderer);
	}

	f2d_FreeFight(fight);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	return 0;
}
