﻿# CMakeList.txt : CMake project for FightingGame2D, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

# Add source to this project's executable.
add_executable (client "main.cpp")

target_link_libraries(client fight2d)

find_package(SDL2 CONFIG REQUIRED)
target_link_libraries(client SDL2::SDL2 SDL2::SDL2main)