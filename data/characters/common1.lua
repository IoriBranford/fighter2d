return {
	states = {
		stand = {
			type = "S",
			physics = "S",
			sprpriority = 0,
			control = function()
				if Anim ~= 0 and Anim ~= 5 or Anim == 5 and AnimTime == 0 then
					ChangeAnim("stand")
				end
				if Time == 0 then
					VelSet(nil, 0)
				end
				if abs(vel x) < Const(movement.stand.friction.threshold) or Time = 4 then
					VelSet(0, nil)
				end
				if not alive then
					return "air_get_hit"
				end
			end
		},
		stand_to_crouch = {
			type = "C",
			physics = "C",
			anim = "stand_to_crouch",
			control = function()
				if Time == 0 then
					VelMul(.75, nil)
				end
				if abs(vel x) < Const(movement.crouch.friction.threshold) then
					VelSet(0, nil)
				end
				if AnimTime == 0 then
					return "crouch"
				end
			end
		},
		crouch = {
			type    = "C",
			physics = "C",
			anim = "crouch",
			sprpriority = 0,
			control = function()
				if Anim == "turn" and AnimTime == 0 then
					ChangeAnim("crouch")
				end
				if abs(vel x) < Const(movement.crouch.friction.threshold) then
					VelSet(0, nil)
				end
			end
		},
		crouch_to_stand = {
			type = "S",
			physics = "S",
			control = function()
				if AnimTime == 0 then
					return "stand"
				end
			end
		}
	}
}